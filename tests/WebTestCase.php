<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 * @package        MailboxApi\Tests
 */

namespace MailboxApi\Tests;

class WebTestCase extends \Silex\WebTestCase
{
    public function createApplication()
    {
        return require __DIR__ . '/../src/app.php';
    }

    protected function getClient()
    {
        return $this->createClient();
    }
}
