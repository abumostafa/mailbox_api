<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 * @package        MailboxApi\Entity
 */

namespace MailboxApi\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="MailboxApi\Repository\UserRepository")
 * @ORM\Table(name="users")
 *
 * @Serializer\ExclusionPolicy("ALL")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(length=255)
     *
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="MessageUser", mappedBy="user", cascade={"remove"})
     */
    private $messageUsers;

    public function __construct()
    {
        $this->messageUsers = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getMessageUsers()
    {
        return $this->messageUsers;
    }

    public function addMessageUser(MessageUser $userMessage)
    {
        if (!$this->messageUsers->contains($userMessage)) {
            $this->messageUsers->add($userMessage);
        }

        return $this;
    }

    public function removeMessageUser(MessageUser $userMessage)
    {
        if ($this->messageUsers->contains($userMessage)) {
            $this->messageUsers->removeElement($userMessage);
        }

        return $this;
    }
}
