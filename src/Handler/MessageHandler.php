<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 * @package        MailboxApi\Handler
 */

namespace MailboxApi\Handler;

use MailboxApi\Entity\Message;
use MailboxApi\Request\MessageListRequestParams;
use MailboxApi\Repository\MessageRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Message Handler
 *
 * Handle incoming requests from the controller
 *
 * @package MailboxApi\Handler
 */
class MessageHandler
{
    /**
     * @var MessageRepository
     */
    private $messageRepository;

    /**
     * MessageHandler constructor.
     * @param MessageRepository $messageRepository
     */
    public function __construct(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    /**
     * @param $messageId
     * @return Message
     */
    public function handleShowAction($messageId)
    {
        return $this->findOneOrFailBy(['id' => $messageId]);
    }

    /**
     *
     * @param $messageId
     * @param array $data
     * @return Message
     */
    public function handlePatchAction($messageId, array $data)
    {
        $message = $this->findOneOrFailBy(['id' => $messageId]);

        if (!empty($data['is_read']) && $data['is_read']) {
            $this->messageRepository->markAsRead($message);
        }

        if (!empty($data['is_archived']) && $data['is_archived']) {
            $this->messageRepository->markAsArchived($message);
        }

        return $message;
    }

    public function getRepository()
    {
        return $this->messageRepository;
    }

    /**
     * @param array $criteria
     * @return Message
     */
    protected function findOneOrFailBy(array $criteria)
    {
        /** @var Message $message */
        if ($message = $this->messageRepository->findOneBy($criteria)) {
            return $message;
        }

        throw new NotFoundHttpException('Message not found');
    }
}
