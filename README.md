# Mailbox API

###NOTE:
***```This code is not production ready. it's a concept of Restful APIs and HATEOAS standerds```***

The Mailbox API is very simple Rest API based on [Silex](https://silex.symfony.com/) framework and some other open source libraries.
Mailbox API is limited for it's first version.and more features will be added in the future. 

####Libraries
- [Silex](https://silex.symfony.com/)
- [Doctrine ORM](http://www.doctrine-project.org/)
- [HATEOAS](https://github.com/willdurand/Hateoas)
- [PHPUnit](https://phpunit.de/)

####Installation
- Create a config file `cp config/config.oho.dist config/config.php`
- Update database connection and configuration inside `config/config.php`
- Run `composer install`
- Run `bin/prepare.sh` will create the database tables and insert sample data

####Start Your Server
***```php -S 0.0.0.0:3333 web/index.php```***

####Tests
***```php vendor/bin/phpunit tests```***

####Sample Data
if you run bin/prepare.sh it will insert sample data already shipped with the API. but you also able to run the command manual ***```php bin/console message/import path/to/file.json```*** ***make sure to have `messages` namespace as wrapper for the messages in the file and also the message keys should be same as the sample file***


#### Supported Calls:
- [List unarchived messages](#unarchived-messages)
- [List archived messages](#archived-messages)
- [Fetch single message](#fetch-message)
- [mark message as read](#mark-as-read)
- [mark message as archived](#archive-message)

#####List unarchived messages
You can fetch all unarchived messages paginated by simple GET http call. *please see the request example below*
***```curl -X GET -H "Content-Type: application/json" -u owner:foo  "http://0.0.0.0:3333/v1/message"```***

and here is the response example
```json
{
    "page": 1,
    "limit": 10,
    "pages": 1,
    "total": 5,
    "_links": {
        "self": {
            "href": "/v1/message?page=1&limit=10"
        },
        "first": {
            "href": "/v1/message?page=1&limit=10"
        },
        "last": {
            "href": "/v1/message?page=1&limit=10"
        }
    },
    "_embedded": {
        "items": [
            {
                "read_at": "2017-10-24T23:27:58+0000",
                "is_read": true,
                "archived_at": null,
                "is_archived": false,
                "id": 21,
                "subject": "animals",
                "content": "This is a tale about nihilism. The story is about a combative nuclear engineer who hates animals. It starts in a ghost town on a world of forbidden magic. The story begins with a legal dispute and ends with a holiday celebration.",
                "sent_at": "2016-03-29T08:24:27+0000",
                "sender": {
                    "name": "Ernest Hemingway"
                }
            },
            ...
        ]
    }
}
```

#####List archived messages
You can fetch all archived messages paginated by simple GET http call. *please see the request example below*
***```curl -X GET -H "Content-Type: application/json" -u owner:foo  "http://0.0.0.0:3333/v1/message?is_archived=1"```***

and here is the response example
```json
{
    "page": 1,
    "limit": 10,
    "pages": 1,
    "total": 1,
    "_links": {
        "self": {
            "href": "/v1/message?page=1&is_archived=1&limit=10"
        },
        "first": {
            "href": "/v1/message?page=1&is_archived=1&limit=10"
        },
        "last": {
            "href": "/v1/message?page=1&is_archived=1&limit=10"
        }
    },
    "_embedded": {
        "items": [
            {
                "read_at": null,
                "is_read": false,
                "archived_at": "2017-10-24T23:33:41+0000",
                "is_archived": true,
                "id": 22,
                "subject": "adoration",
                "content": "The story is about a fire fighter, a naive bowman, a greedy fisherman, and a clerk who is constantly opposed by a heroine. It takes place in a small city. The critical element of the story is an adoration.",
                "sent_at": "2016-03-29T10:52:27+0000",
                "sender": {
                    "name": "Stephen King"
                }
            }
        ]
    }
}
```

#####Fetch single message
You can fetch a single message by simple GET http call.
***```curl -X GET -H "Content-Type: application/json" -u owner:foo  "http://0.0.0.0:3333/v1/message/21"```***

and here is the response example
```json
{
  "read_at": "2017-10-24T23:27:58+0000",
  "is_read": true,
  "archived_at": null,
  "is_archived": false,
  "id": 21,
  "subject": "animals",
  "content": "This is a tale about nihilism. The story is about a combative nuclear engineer who hates animals. It starts in a ghost town on a world of forbidden magic. The story begins with a legal dispute and ends with a holiday celebration.",
  "sent_at": "2016-03-29T08:24:27+0000",
  "sender": {
    "name": "Ernest Hemingway"
  },
  "_links": {
    "self": {
      "href": "/v1/message/21"
    }
  }
}
```

#####mark message as read
You can mark a single message as read by simple PATCH http call.
***```curl -X PATCH -H "Content-Type: application/json" -u owner:foo  -d '{"is_read": true}' "http://0.0.0.0:3333/v1/message/21"```***

and here is the response example
```json
{
  "read_at": "2017-10-24T23:27:58+0000",
  "is_read": true,
  "archived_at": null,
  "is_archived": false,
  "id": 21,
  "subject": "animals",
  "content": "This is a tale about nihilism. The story is about a combative nuclear engineer who hates animals. It starts in a ghost town on a world of forbidden magic. The story begins with a legal dispute and ends with a holiday celebration.",
  "sent_at": "2016-03-29T08:24:27+0000",
  "sender": {
    "name": "Ernest Hemingway"
  },
  "_links": {
    "self": {
      "href": "/v1/message/21"
    }
  }
}
```

#####mark message as archived
You can archive a single message by simple PATCH http call.
***```curl -X PATCH -H "Content-Type: application/json" -u owner:foo  -d '{"is_archived": true}' "http://0.0.0.0:3333/v1/message/21"```***

and here is the response example
```json
{
  "read_at": "2017-10-24T23:27:58+0000",
  "is_read": true,
  "archived_at": "2017-10-24T23:27:58+0000",
  "is_archived": true,
  "id": 21,
  "subject": "animals",
  "content": "This is a tale about nihilism. The story is about a combative nuclear engineer who hates animals. It starts in a ghost town on a world of forbidden magic. The story begins with a legal dispute and ends with a holiday celebration.",
  "sent_at": "2016-03-29T08:24:27+0000",
  "sender": {
    "name": "Ernest Hemingway"
  },
  "_links": {
    "self": {
      "href": "/v1/message/21"
    }
  }
}
```
