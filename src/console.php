<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 */

use Bezhanov\Silex\AliceDataFixtures\FixturesServiceProvider;
use Kurl\Silex\Provider\DoctrineMigrationsProvider;

$app = require dirname(__DIR__) . '/src/app.php';

$console = new Symfony\Component\Console\Application;

$app->register(new \MailboxApi\Provider\ConsoleCommandServiceProvider($console));
$app->register(new DoctrineMigrationsProvider($console), [
    'migrations.directory' => $app['app.root'] . '/database/Migration',
    'migrations.namespace' => 'MailboxApi\\Migration',
]);

$app->register(new FixturesServiceProvider($console));
$app->boot(); // manual booting to add the console commands

return $console;
