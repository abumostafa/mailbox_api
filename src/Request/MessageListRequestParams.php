<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 * @package        MailboxApi\Request
 */

namespace MailboxApi\Request;

use MailboxApi\Request\AbstractRequestParams;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MessageListRequestParams extends AbstractRequestParams
{
    /**
     * @param OptionsResolver $resolver
     */
    protected function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefined('page')
            ->setDefined('is_archived')

            ->setAllowedTypes('page', ['scalar'])
            ->setAllowedTypes('is_archived', ['scalar'])

            ->setDefault('page', 1)
            ->setDefault('is_archived', 0);
    }
}
