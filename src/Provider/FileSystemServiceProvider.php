<?php namespace MailboxApi\Provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Symfony\Component\Filesystem\Filesystem;

class FileSystemServiceProvider implements ServiceProviderInterface
{
    /**
     * @param Container $pimple
     */
    public function register(Container $pimple)
    {
        $pimple['filesystem'] = function (Container $pimple) {
            return new Filesystem();
        };
    }
}