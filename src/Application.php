<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 * @package        MailboxApi
 */

namespace MailboxApi;

use Dflydev\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use MailboxApi\Provider\AppServiceProvider;
use MailboxApi\Provider\FileSystemServiceProvider;
use MailboxApi\Voter\MessageVoter;
use Monolog\Logger;
use Silex\Application as SilexApplication;
use Silex\ControllerCollection;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\RoutingServiceProvider;
use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Symfony\Component\HttpFoundation\Request;

class Application extends SilexApplication
{
    public function __construct(array $values = array())
    {
        parent::__construct($values);

        $this->registerServiceProviders();
        $this->registerControllers();
    }

    protected function registerServiceProviders()
    {
        $app = $this;

        $app
            ->register(new ServiceControllerServiceProvider())
            ->register(new RoutingServiceProvider())
            ->register(new ValidatorServiceProvider())
            ->register(new FileSystemServiceProvider())
            ->register(new DoctrineServiceProvider(), [
                'db.options' => $app['config']['database'],
            ])
            ->register(new DoctrineOrmServiceProvider(), [
                'orm.proxies_dir' => $app['app.root'] . $app['config']['dirs.cache'] . '/proxy',
                'orm.em.options' => [
                    'mappings' => [
                        [
                            'type' => 'annotation',
                            'namespace' => 'MailboxApi\Entity',
                            'path' => $app['app.root'] . '/src/Entity',
                            'use_simple_annotation_reader' => false,
                        ],
                    ],
                ],
            ])
            ->register(new MonologServiceProvider(), [
                'monolog.not_found_activation_strategy' => Logger::INFO,
                'monolog.logfile' => $app['app.root'] . '/' . $app['config']['dirs.log'] . '/' . $app['config']['app.environment'] . '.log',
            ])
            ->register(new SecurityServiceProvider(), [
                'security.firewalls' => $app['config']['security.firewalls']
            ])
            ->register(new AppServiceProvider())
            ->extend('security.voters', function ($voters) use ($app) {
                $voters[] = new MessageVoter();
                return $voters;
            });
    }

    protected function registerControllers()
    {
        $app = $this;

        /** @var ControllerCollection $controllers */
        $controllers = $app['controllers_factory'];

        $controllers
            ->get('/message', 'mailbox_api.controller.message:listAction')
            ->before(function (Request $request, Application $app) {
                $app['mailbox_api.middleware.acl']->grantAccessListAction($request, $app);
            })
            ->bind('message.list');

        $controllers
            ->get('/message/{messageId}', 'mailbox_api.controller.message:showAction')
            ->before(function (Request $request, Application $app) {
                $app['mailbox_api.middleware.acl']->grantAccessShowAction($request, $app);
            })
            ->bind('message.show');

        $controllers
            ->patch('/message/{messageId}', 'mailbox_api.controller.message:patchAction')
            ->before(function (Request $request, Application $app) {
                $app['mailbox_api.middleware.acl']->grantAccessEditAction($request, $app);
            })
            ->bind('message.patch');

        $app->mount('/v1', $controllers);
    }
}