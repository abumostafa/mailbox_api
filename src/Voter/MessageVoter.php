<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 * @package        MailboxApi\Voter
 */

namespace MailboxApi\Voter;

use MailboxApi\Entity\Message;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\User;

/**
 * Message Voter
 *
 * @package MailboxApi\Voter
 */
class MessageVoter extends Voter
{
    const VIEW = 'VIEW';

    const EDIT = 'EDIT';

    /**
     * @inheritdoc
     */
    protected function supports($attribute, $message)
    {
        if (!in_array($attribute, [self::VIEW, self::EDIT])) {
            return false;
        }

        if (!$message instanceof Message) {
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    protected function voteOnAttribute($attribute, $message, TokenInterface $token)
    {
        if (!$token->getUser() instanceof User) {
            return false;
        }

        return true;
    }
}
