<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 * @package        MailboxApi\Tests\MessageBundle\Controller
 */

namespace MailboxApi\Tests\MessageBundle\Controller;

use MailboxApi\Tests\WebTestCase;

class MessageControllerTest extends WebTestCase
{
    protected $ownerCredentials = [
        'PHP_AUTH_USER' => 'owner',
        'PHP_AUTH_PW' => 'foo'
    ];


    protected $guestCredentials = [
        'PHP_AUTH_USER' => 'guest',
        'PHP_AUTH_PW' => 'foo'
    ];

    /*
     * List Action Test
     */
    public function testListActionUnauthenticated()
    {
        $client = $this->createClient();
        $client->request('GET', '/v1/message');

        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }

    public function testListActionUnauthorized()
    {
        $client = $this->createClient();
        $client->request('GET', '/v1/message', [], [], $this->guestCredentials);
        $this->assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testListActionUnArchived()
    {
        $client = $this->createClient();
        $client->request('GET', '/v1/message', [], [], $this->ownerCredentials);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $content = json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('page', $content);
        $this->assertArrayHasKey('limit', $content);
        $this->assertArrayHasKey('pages', $content);
        $this->assertArrayHasKey('total', $content);
        $this->assertArrayHasKey('_links', $content);
        $this->assertArrayHasKey('self', $content['_links']);
        $this->assertArrayHasKey('first', $content['_links']);
        $this->assertArrayHasKey('last', $content['_links']);
        $this->assertArrayHasKey('_embedded', $content);
        $this->assertArrayHasKey('items', $content['_embedded']);

        foreach ($content['_embedded']['items'] as $message) {
            $this->assertMessageIsOk($message);
            $this->assertFalse($message['is_archived']);
            $this->assertNull($message['archived_at']);
        }
    }

    public function testListActionArchived()
    {
        $client = $this->createClient();
        $client->request('GET', '/v1/message?is_archived=1', [], [], $this->ownerCredentials);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $content = json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('page', $content);
        $this->assertArrayHasKey('limit', $content);
        $this->assertArrayHasKey('pages', $content);
        $this->assertArrayHasKey('total', $content);
        $this->assertArrayHasKey('_links', $content);
        $this->assertArrayHasKey('self', $content['_links']);
        $this->assertArrayHasKey('first', $content['_links']);
        $this->assertArrayHasKey('last', $content['_links']);
        $this->assertArrayHasKey('_embedded', $content);
        $this->assertArrayHasKey('items', $content['_embedded']);

        foreach ($content['_embedded']['items'] as $message) {
            $this->assertMessageIsOk($message);
            $this->assertTrue($message['is_archived']);
            $this->assertNotNull($message['archived_at']);
        }
    }

    public function testReadMessage()
    {
        // mark message as read
        $client = $this->createClient();
        $client->setServerParameter('CONTENT_TYPE' ,'application/json');
        $client->request('PATCH', '/v1/message/21', [], [], $this->ownerCredentials, json_encode(['is_read' => true]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $readMessage = $this->decode($client->getResponse()->getContent());

        $this->assertMessageIsOk($readMessage);
        $this->assertTrue($readMessage['is_read']);
    }

    public function testArchiveMessage()
    {
        // mark message as read
        $client = $this->createClient();
        $client->setServerParameter('CONTENT_TYPE' ,'application/json');
        $client->request('PATCH', '/v1/message/22', [], [], $this->ownerCredentials, json_encode(['is_archived' => true]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $readMessage = $this->decode($client->getResponse()->getContent());

        $this->assertMessageIsOk($readMessage);
        $this->assertTrue($readMessage['is_archived']);
    }

    protected function assertMessageIsOk(array $message)
    {
        $this->assertArrayHasKey('id', $message);
        $this->assertArrayHasKey('subject', $message);
        $this->assertArrayHasKey('content', $message);
        $this->assertArrayHasKey('sender', $message);
        $this->assertArrayHasKey('name', $message['sender']);
        $this->assertArrayHasKey('sent_at', $message);
        $this->assertArrayHasKey('is_read', $message);
        $this->assertArrayHasKey('read_at', $message);
        $this->assertArrayHasKey('is_archived', $message);
        $this->assertArrayHasKey('archived_at', $message);
    }

    protected function getMessageById($id)
    {
        $client = $this->createClient();
        $client->request('GET', sprintf('/v1/message/%s', $id), [], [], $this->ownerCredentials);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        return $this->decode($client->getResponse()->getContent());
    }

    protected function decode($json)
    {
        return json_decode($json, true);
    }
}