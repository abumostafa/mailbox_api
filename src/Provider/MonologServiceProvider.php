<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 * @package        MailboxApi\Provider
 */

namespace MailboxApi\Provider;

use Monolog\Formatter\LineFormatter;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Pimple\Container;

class MonologServiceProvider extends \Silex\Provider\MonologServiceProvider
{
    public function register(Container $pimple)
    {
        parent::register($pimple);

        $pimple['monolog.formatter'] = function () {
            return new LineFormatter("%channel%[%extra.uid%].%level_name%: %message% %context% %extra%\n");
        };

        $pimple['monolog'] = $pimple->extend('monolog', function (Logger $monolog) use ($pimple) {
            $monolog->pushProcessor($pimple['monolog.processor.uid']);

            return $monolog;
        });

        $pimple['monolog.processor.uid'] = function () {
            return new UidProcessor();
        };
    }
}
