<?php namespace MailboxApi\Command;

use MailboxApi\Entity\User;
use MailboxApi\Repository\MessageRepository;
use MailboxApi\Repository\MessageUserRepository;
use MailboxApi\Repository\UserRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class ImportMessages extends Command
{
    const FORMAT_JSON = 'json';

    const JSON_NAMESPACE = 'messages';

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var MessageRepository
     */
    private $messageRepository;
    /**
     * @var MessageUserRepository
     */
    private $messageUserRepository;

    /**
     * ImportMessages constructor.
     * @param Filesystem $filesystem
     * @param UserRepository $userRepository
     * @param MessageRepository $messageRepository
     * @param MessageUserRepository $messageUserRepository
     */
    public function __construct(Filesystem $filesystem,
                                UserRepository $userRepository,
                                MessageRepository $messageRepository,
                                MessageUserRepository $messageUserRepository)
    {
        parent::__construct();

        $this->filesystem = $filesystem;
        $this->userRepository = $userRepository;
        $this->messageRepository = $messageRepository;
        $this->messageUserRepository = $messageUserRepository;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('message:import')
            ->setDescription('Import messages')
            ->addArgument('path', InputArgument::REQUIRED, 'File path')
            ->addArgument('format', InputArgument::OPTIONAL, 'File Format', 'json');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $format = $input->getArgument('format');
        $path = $input->getArgument('path');

        $raw = $this->getContent($path);

        switch (strtolower($format)) {
            case self::FORMAT_JSON:
                $this->importFromJSON($raw);
                break;

            default:
                throw new \Exception(sprintf('Format "%s" is not supported', $format));

        }
    }

    /**
     * @param $path
     * @return string
     * @throws \Exception
     */
    protected function getContent($path)
    {
        $fullPath = null;

        if ($this->filesystem->exists($path)) {
            $fullPath = $path;
        }

        $baseDir = dirname(__FILE__);

        if ($this->filesystem->exists($baseDir . '/' . $path)) {
            $fullPath = $baseDir . '/' . $path;
        }

        if (!$fullPath) {
            throw new \InvalidArgumentException(sprintf('File isn\'t exist "%s"', $path));
        }

        if (!is_readable($fullPath)) {
            throw new \InvalidArgumentException(sprintf('File isn\'t readable "%s"', $path));
        }

        $content = file_get_contents($fullPath);

        if (false === $content) {
            throw new \Exception('Failed to fetch file content');
        }

        return $content;
    }

    protected function importFromJSON($raw)
    {
        $content = json_decode($raw, true);

        if (json_last_error()) {
            throw new \Exception(json_last_error_msg());
        }

        if (!array_key_exists(self::JSON_NAMESPACE, $content)) {
            throw new \Exception(sprintf('JSON namespace "%s" is not exists', self::JSON_NAMESPACE));
        }

        foreach ($content[self::JSON_NAMESPACE] as $item) {
            $this->insertMessage($item);
        }
    }

    /**
     * @param array $message
     * @throws \Exception
     */
    protected function insertMessage(array $message)
    {
        $header = ['uid', 'sender', 'subject', 'message', 'time_sent'];
        $missingKeys = array_diff($header, array_keys($message));

        if (count($missingKeys) > 0) {
            throw new \Exception('Message keys "(%s)" are missing', implode(',', $missingKeys));
        }

        $senderEntity = $this->userRepository->findOrCreateBy(['name' => $message['sender']]);

        $messageEntity = $this->messageRepository->createMessage([
            'id' => $message['uid'],
            'subject' => $message['subject'],
            'content' => $message['message'],
            'sender' => $senderEntity,
            'sent_at' => (new \DateTime())->setTimestamp($message['time_sent']),
        ]);

        /* message should have at least one receiver. so let's fake it */
        $this->messageUserRepository->connectMessageToUser($messageEntity, $senderEntity);
    }
}
