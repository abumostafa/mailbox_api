<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 * @package        MailboxApi\Request
 */

namespace MailboxApi\Request;

use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 *
 *
 * @package MailboxApi\Request
 */
abstract class AbstractRequestParams
{
    /**
     * @var array
     */
    protected $input = [];

    /**
     * @var array
     */
    protected $params = [];

    /**
     * MessageListRequestParams constructor.
     * @param array $input
     */
    public function __construct(array $input = [])
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);

        $cleanInput = array_filter($input, function ($key) use ($resolver) {
            return in_array($key, $resolver->getDefinedOptions());
        }, ARRAY_FILTER_USE_KEY);

        $this->params = $resolver->resolve($cleanInput);
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param string $name
     * @param null $default
     * @return mixed|null
     */
    public function getParam($name, $default = null)
    {
        return array_key_exists($name, $this->params) ? $this->params[$name] : $default;
    }

    abstract protected function configureOptions(OptionsResolver $resolver);
}
