<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 * @package        MailboxApi\Entity
 */

namespace MailboxApi\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MailboxApi\Repository\MessageUserRepository")
 * @ORM\Table(name="users_to_messages")
 */
class MessageUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="Messages")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=FALSE)
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Message", inversedBy="users")
     * @ORM\JoinColumn(name="message_id", referencedColumnName="id", nullable=FALSE)
     */
    protected $message;

    /**
     * @ORM\Column(name="is_read", type="boolean")
     */
    protected $isRead = false;

    /**
     * @ORM\Column(name="read_at", type="datetime", nullable=true)
     */
    protected $readAt;

    /**
     * @ORM\Column(name="is_archived", type="boolean")
     */
    protected $isArchived = false;

    /**
     * @ORM\Column(name="archived_at", type="datetime", nullable=true)
     */
    protected $archivedAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     * @return $this
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        if ($this->user !== null) {
            $this->user->removeMessageUser($this);
        }

        if ($user !== null) {
            $user->addMessageUser($this);
        }

        $this->user = $user;
        return $this;
    }

    /**
     * @return Message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param $message
     * @return $this
     */
    public function setMessage(Message $message)
    {
        if ($this->message !== null) {
            $this->message->removeMessageUser($this);
        }

        if ($message !== null) {
            $message->addMessageUser($this);
        }

        $this->message = $message;
        return $this;
    }

    /**
     * @return mixed
     */
    public function isRead()
    {
        return $this->isRead;
    }

    /**
     * @param mixed $isRead
     */
    public function setIsRead($isRead)
    {
        $this->isRead = $isRead;
    }

    /**
     * @return mixed
     */
    public function getReadAt()
    {
        return $this->readAt;
    }

    /**
     * @param mixed $readAt
     */
    public function setReadAt($readAt)
    {
        $this->readAt = $readAt;
    }

    /**
     * @return mixed
     */
    public function isArchived()
    {
        return $this->isArchived;
    }

    /**
     * @param mixed $isArchived
     */
    public function setIsArchived($isArchived)
    {
        $this->isArchived = $isArchived;
    }

    /**
     * @return mixed
     */
    public function getArchivedAt()
    {
        return $this->archivedAt;
    }

    /**
     * @param mixed $archivedAt
     */
    public function setArchivedAt($archivedAt)
    {
        $this->archivedAt = $archivedAt;
    }
}
