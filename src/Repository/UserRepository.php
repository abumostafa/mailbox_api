<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 * @package        MailboxApi\Repository
 */

namespace MailboxApi\Repository;

use Doctrine\ORM\EntityRepository;
use MailboxApi\Entity\User;

class UserRepository extends EntityRepository
{
    /**
     * @param array $criteria
     * @return User|null|object
     */
    public function findOrCreateBy(array $criteria)
    {
        if ($user = $this->findOneBy($criteria)) {
            return $user;
        }

        return $this->createUser($criteria);
    }

    /**
     * @param array $data
     * @return User
     */
    public function createUser(array $data)
    {
        $user = new User();

        if (!empty($data['name'])) {
            $user->setName($data['name']);
        }

        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();

        return $user;
    }
}