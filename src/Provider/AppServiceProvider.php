<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 * @package        MailboxApi\Provider
 */

namespace MailboxApi\Provider;

use Hateoas\HateoasBuilder;
use Hateoas\UrlGenerator\SymfonyUrlGenerator;
use MailboxApi\Entity\Message;
use MailboxApi\Controller\MessageController;
use MailboxApi\Handler\MessageHandler;
use MailboxApi\Middleware\MessageAcl;
use MailboxApi\Support\Serializer\JSONSerializer;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\HttpFoundation\Request;

/**
 * App Service Provider
 *
 * register and connect dependencies
 *
 * @package MailboxApi
 */
class AppServiceProvider implements ServiceProviderInterface, ControllerProviderInterface
{
    /**
     * @param Application $app
     * @return ControllerCollection
     */
    public function connect(Application $app)
    {
        /** @var ControllerCollection $controllers */
        $controllers = $app['controllers_factory'];

        $controllers
            ->get('/', 'mailbox_api.controller.message:listAction')
            ->before(function (Request $request, Application $app) {
                $app['mailbox_api.middleware.acl']->grantAccessListAction($request, $app);
            })
            ->bind('message.list');

        $controllers
            ->get('/{messageId}', 'mailbox_api.controller.message:showAction')
            ->before(function (Request $request, Application $app) {
                $app['mailbox_api.middleware.acl']->grantAccessShowAction($request, $app);
            })
            ->bind('message.show');

        $controllers
            ->patch('/{messageId}', 'mailbox_api.controller.message:patchAction')
            ->before(function (Request $request, Application $app) {
                $app['mailbox_api.middleware.acl']->grantAccessEditAction($request, $app);
            })
            ->bind('message.patch');

        return $controllers;
    }

    /**
     * @param Container $pimple
     */
    public function register(Container $pimple)
    {
        $pimple['serializer'] = function (Container $pimple) {
            return HateoasBuilder::create()
                ->setUrlGenerator(null, new SymfonyUrlGenerator($pimple['url_generator']))
                ->setCacheDir($pimple['app.root'] . '/' . $pimple['config']['dirs.cache'] . '/' . 'serializer')
                ->build();
        };

        $pimple['mailbox_api.middleware.acl'] = function (Container $pimple) {
            return new MessageAcl(
                $pimple['orm.ems']['default'],
                $pimple['security.authorization_checker']
            );
        };

        $pimple['mailbox_api.handler.message'] = function (Container $pimple) {
            return new MessageHandler(
                $pimple['orm.ems']['default']->getRepository(Message::class)
            );
        };

        $pimple['mailbox_api.controller.message'] = function (Container $pimple) {
            return new MessageController(
                $pimple['serializer'],
                $pimple['mailbox_api.handler.message']
            );
        };
    }
}
