<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 * @package        MailboxApi\Provider
 */

namespace MailboxApi\Provider;

use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use MailboxApi\Entity\Message;
use MailboxApi\Entity\MessageUser;
use MailboxApi\Entity\User;
use MailboxApi\Command\ImportMessages;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Symfony\Component\Console\Application as Console;
use Symfony\Component\Console\Helper\HelperSet;

class ConsoleCommandServiceProvider implements ServiceProviderInterface
{
    /**
     * @var Console
     */
    private $console;

    /**
     * ConsoleCommandServiceProvider constructor.
     * @param Console $console
     */
    public function __construct(Console $console)
    {
        $this->console = $console;
    }

    public function register(Container $pimple)
    {
        $helperSet = new HelperSet(array(
            'connection' => new ConnectionHelper($pimple['db']),
        ));

        $helperSet->set(new EntityManagerHelper($pimple['orm.ems']['default']), 'em');

        $this->console->setHelperSet($helperSet);
        $this->registerCommands($pimple);
    }

    protected function registerCommands($app)
    {
        $commands = [
            new ImportMessages(
                $app['filesystem'],
                $app['orm.ems']['default']->getRepository(User::class),
                $app['orm.ems']['default']->getRepository(Message::class),
                $app['orm.ems']['default']->getRepository(MessageUser::class)
            ),
        ];

        $this->console->addCommands($commands);
    }
}