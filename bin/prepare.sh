#!/bin/sh

CURRENT=`pwd`

php ${CURRENT}/bin/console migrations:diff --quiet
php ${CURRENT}/bin/console migrations:migrate --quiet
php ${CURRENT}/bin/console message:import ${CURRENT}/resources/data/messages_sample.json