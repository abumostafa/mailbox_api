<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 * @package        MailboxApi\Entity
 */

namespace MailboxApi\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="MailboxApi\Repository\MessageRepository")
 * @ORM\Table(name="messages")
 *
 * @Serializer\ExclusionPolicy("ALL")
 */
class Message
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\Column(length=255)
     *
     * @Serializer\Expose()
     */
    private $subject;

    /**
     * @ORM\Column(type="text")
     *
     * @Serializer\Expose()
     */
    private $content;

    /**
     * @ORM\Column(name="sent_at", type="datetime")
     *
     * @Serializer\Expose()
     */
    private $sentAt;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="User",
     *     inversedBy="sentMessages"
     * )
     * @ORM\JoinColumn(
     *     name="sender_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     *
     * @Serializer\Expose()
     */
    private $sender;

    /**
     * @ORM\OneToMany(targetEntity="MessageUser", mappedBy="message", cascade={"remove", "persist"})
     */
    protected $messageUsers;

    public function __construct()
    {
        $this->messageUsers = new ArrayCollection();
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }

    /**
     * @param mixed $sentAt
     */
    public function setSentAt($sentAt)
    {
        $this->sentAt = $sentAt;
    }

    /**
     * @return User
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param User $sender
     */
    public function setSender(User $sender)
    {
        $this->sender = $sender;
    }

    public function getMessageUsers()
    {
        return $this->messageUsers;
    }

    public function addMessageUser(MessageUser $messageUser)
    {
        if (!$this->messageUsers->contains($messageUser)) {
            $this->messageUsers->add($messageUser);
        }

        return $this;
    }

    public function removeMessageUser(MessageUser $messageUser)
    {
        if ($this->messageUsers->contains($messageUser)) {
            $this->messageUsers->removeElement($messageUser);
        }

        return $this;
    }

    /**
     * @Serializer\VirtualProperty()
     */
    public function getReadAt()
    {
        return $this->getMessageUsers()->first()->getReadAt();
    }

    /**
     * @Serializer\VirtualProperty()
     */
    public function getIsRead()
    {
        return $this->getMessageUsers()->first()->isRead();
    }

    /**
     * @Serializer\VirtualProperty()
     */
    public function getArchivedAt()
    {
        return $this->getMessageUsers()->first()->getArchivedAt();
    }

    /**
     * @Serializer\VirtualProperty()
     */
    public function getIsArchived()
    {
        return $this->getMessageUsers()->first()->isArchived();
    }
}
