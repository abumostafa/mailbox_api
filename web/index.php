<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 */

/** @var \MailboxApi\Application $app */
$app = require dirname(__FILE__) . '/../src/app.php';
$app->run();