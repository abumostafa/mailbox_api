<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 * @package        MailboxApi\Repository
 */

namespace MailboxApi\Repository;

use Doctrine\ORM\EntityRepository;
use MailboxApi\Entity\Message;
use MailboxApi\Entity\MessageUser;
use MailboxApi\Entity\User;

class MessageUserRepository extends EntityRepository
{
    public function connectMessageToUser(Message $message, User $user)
    {
        $messageUser = new MessageUser();
        $messageUser->setMessage($message);
        $messageUser->setUser($user);

        $this->getEntityManager()->persist($messageUser);
        $this->getEntityManager()->flush();
    }
}