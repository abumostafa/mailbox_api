<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 * @package        MailboxApi\Middleware
 */

namespace MailboxApi\Middleware;

use Doctrine\ORM\EntityManagerInterface;
use MailboxApi\Entity\Message;
use MailboxApi\Voter\MessageVoter;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class MessageAcl
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $checker;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * MessageAcl constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param AuthorizationCheckerInterface $checker
     */
    public function __construct(EntityManagerInterface $entityManager, AuthorizationCheckerInterface $checker)
    {
        $this->entityManager = $entityManager;
        $this->checker = $checker;
    }

    /**
     * @param Request $request
     * @param Application $app
     */
    public function grantAccessListAction(Request $request, Application $app)
    {
        $this->denyAccessUnlessGranted('ROLE_OWNER');
    }

    /**
     * @param Request $request
     * @param Application $app
     */
    public function grantAccessShowAction(Request $request, Application $app)
    {
        $this->denyAccessUnlessGranted('ROLE_OWNER');
    }

    /**
     * @param Request $request
     * @param Application $app
     */
    public function grantAccessEditAction(Request $request, Application $app)
    {
        $this->denyAccessUnlessGranted('ROLE_OWNER');
    }

    /**
     * Checks if the attributes are granted against the current authentication token and optionally supplied object.
     *
     * @param mixed $attributes The attributes
     * @param mixed $object The object
     *
     * @return bool
     *
     * @throws \LogicException
     */
    protected function isGranted($attributes, $object = null)
    {
        return $this->checker->isGranted($attributes, $object);
    }

    /**
     * Throws an exception unless the attributes are granted against the current authentication token and optionally
     * supplied object.
     *
     * @param mixed $attributes The attributes
     * @param mixed $object The object
     * @param string $message The message passed to the exception
     *
     * @throws AccessDeniedException
     */
    protected function denyAccessUnlessGranted($attributes, $object = null, $message = 'Access Denied.')
    {
        if (!$this->isGranted($attributes, $object)) {
            $exception = $this->createAccessDeniedException($message);
            $exception->setAttributes($attributes);
            $exception->setSubject($object);

            throw $exception;
        }
    }

    /**
     * Returns an AccessDeniedException.
     *
     * This will result in a 403 response code. Usage example:
     *
     *     throw $this->createAccessDeniedException('Unable to access this page!');
     *
     * @param string $message A message
     * @param \Exception|null $previous The previous exception
     *
     * @return AccessDeniedException
     */
    protected function createAccessDeniedException($message = 'Access Denied.', \Exception $previous = null)
    {
        return new AccessDeniedException($message, $previous);
    }
}
