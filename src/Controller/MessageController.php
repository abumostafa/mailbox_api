<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 * @package        MailboxApi\Controller
 */

namespace MailboxApi\Controller;

use Hateoas\Configuration\Route;
use Hateoas\Representation\Factory\PagerfantaFactory;
use Hateoas\Representation\RouteAwareRepresentation;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use MailboxApi\Handler\MessageHandler;
use MailboxApi\Request\MessageListRequestParams;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Message Controller
 *
 * @package MailboxApi\Controller
 */
class MessageController
{
    /**
     * @var MessageHandler
     */
    protected $messageHandler;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * MessageController constructor.
     *
     * @param SerializerInterface $serializer
     * @param MessageHandler $messageHandler
     */
    public function __construct(SerializerInterface $serializer, MessageHandler $messageHandler)
    {
        $this->serializer = $serializer;
        $this->messageHandler = $messageHandler;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function listAction(Request $request)
    {
        $requestParams = new MessageListRequestParams($request->query->all());

        $query = $this->messageHandler
            ->getRepository()
            ->getMessagesListQuery($requestParams->getParams());

        $factory = new PagerfantaFactory();
        $adapter = new DoctrineORMAdapter($query);
        $pager = new Pagerfanta($adapter);

        $pager
            ->setCurrentPage((int)$requestParams->getParam('page'))
            ->setMaxPerPage(10);

        $collection = $factory->createRepresentation($pager, new Route('message.list'));

        return $this->createApiResponse($collection);
    }

    /**
     * @param $messageId
     * @return Response
     */
    public function showAction($messageId)
    {
        $message = $this->messageHandler->handleShowAction($messageId);
        $resource = new RouteAwareRepresentation($message, 'message.show', ['messageId' => $messageId]);
        return $this->createApiResponse($resource);
    }

    /**
     * Partial update
     * @param $messageId
     * @param Request $request
     * @return Response
     */
    public function patchAction($messageId, Request $request)
    {
        $message = $this->messageHandler->handlePatchAction($messageId, $request->request->all());
        $resource = new RouteAwareRepresentation($message, 'message.show', ['messageId' => $messageId]);
        return $this->createApiResponse($resource);
    }

    /**
     * @param $resource
     * @param int $status
     * @return Response
     */
    protected function createApiResponse($resource, $status = Response::HTTP_OK)
    {
        $response = new Response('', $status);

        if (!empty($resource)) {

            $context = new SerializationContext();
            $context->setSerializeNull(true);

            $content = $this->serializer->serialize($resource, 'json', $context);
            $response->headers->set('Content-Type', 'application/hal+json');
            $response->setContent($content);
        }

        return $response;
    }
}
