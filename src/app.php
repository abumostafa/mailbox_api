<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 */

use Doctrine\Common\Annotations\AnnotationRegistry;
use Hateoas\Representation\VndErrorRepresentation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;

$loader = require __DIR__ . '/../vendor/autoload.php';

AnnotationRegistry::registerLoader('class_exists');

$config = require dirname(__FILE__) . '/../config/config.php';

$app = new \MailboxApi\Application([
    'app.root' => realpath(dirname(__FILE__) . '/../'),
    'config' => $config,
    'debug' => $config['debug'],
]);

$app->before(function (Request $request) {

    if ($request->getContent()) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});

$app->on(KernelEvents::VIEW, function (GetResponseForControllerResultEvent $event) use ($app) {

    $result = $event->getControllerResult();

    if (empty($result)) {
        $event->setResponse(new Response(''));
    }

    if (is_array($result) || is_object($result)) {

        /** @var \MailboxApi\Contract\SerializerInterface $serializer */
        $serializer = $app['serializer'];

        $content = $serializer->serialize($result);

        if (!$event->hasResponse()) {
            $response = new Response();
            $event->setResponse($response);
        };

        $event->getResponse()->setContent($content);
        $event->getResponse()->headers->add(['Content-Type' => 'application/json']);
    }
});

$app->error(function (\Exception $ex) use ($app) {

    /** @var \JMS\Serializer\SerializerInterface $serializer */
    $serializer = $app['serializer'];

    /** @var \Psr\Log\LoggerInterface $logger */
    $logger = $app['monolog'];

    $logger->error(sprintf("%s: %s (uncaught exception) at %s line %s. %s", get_class($ex), $ex->getMessage(), $ex->getFile(), $ex->getCode(), $ex->getTraceAsString()));

    $error = new VndErrorRepresentation($ex->getMessage());
    $content = $serializer->serialize(['error' => $error], 'json');

    $statusCode = ($ex instanceof HttpException) ? $ex->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;

    $response = new Response($content, $statusCode);
    $response->headers->add(['Content-Type' => 'application/json']);

    return $response;
});

return $app;
