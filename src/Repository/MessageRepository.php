<?php

/**
 * @author         Ahmed Abumostafa <iam.abumostafa@gmail.com>
 * @project        MailboxApi
 * @package        MailboxApi\Repository
 */

namespace MailboxApi\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use MailboxApi\Entity\Message;

/**
 * Message Repository
 *
 * @package MailboxApi\Repository
 */
class MessageRepository extends EntityRepository
{
    /**
     * @param array $filters
     * @return QueryBuilder
     */
    public function getMessagesListQuery(array $filters)
    {
        $queryBuilder = $this
            ->createQueryBuilder('m')
            ->select(['m'])
            ->join('m.messageUsers', 'mu')
            ->where('1 = 1'); // just to allow andWhere

        $this->applyQueryFilters($queryBuilder, $filters);

        return $queryBuilder;
    }

    /**
     * @param QueryBuilder $query
     * @param array $filters
     */
    protected function applyQueryFilters(QueryBuilder $query, array $filters)
    {
        foreach ($filters as $name => $value) {

            switch (strtolower($name)) {

                case 'is_archived':
                    $query
                        ->andWhere('mu.isArchived = :archived')
                        ->setParameter('archived', $value);
                    break;
            }

        }
    }

    /**
     * @param Message $message
     */
    public function markAsRead(Message $message)
    {
        $userMessage = $message->getMessageUsers()->first();

        if (false === $userMessage->isRead()) {

            $userMessage->setIsRead(true);
            $userMessage->setReadAt(new \DateTime());
            $message->addMessageUser($userMessage);

            $this->getEntityManager()->persist($userMessage);
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param Message $message
     */
    public function markAsArchived(Message $message)
    {
        $userMessage = $message->getMessageUsers()->first();

        if (false === $userMessage->isArchived()) {

            $userMessage->setIsArchived(true);
            $userMessage->setArchivedAt(new \DateTime());
            $message->addMessageUser($userMessage);

            $this->getEntityManager()->persist($message);
            $this->getEntityManager()->flush();
        }
    }

    public function createMessage(array $data)
    {
        $message = new Message();

        if (isset($data['id'])) {

            if ($existingMessage = $this->findOneBy(['id' => $data['id']])) {
                $message = $existingMessage;
            } else {
                $message->setId($data['id']);
            }
        }

        if (isset($data['subject'])) {
            $message->setSubject($data['subject']);
        }

        if (isset($data['content'])) {
            $message->setContent($data['content']);
        }

        if (isset($data['sender'])) {
            $message->setSender($data['sender']);
        }

        if (isset($data['sent_at'])) {
            $message->setSentAt($data['sent_at']);
        }

        $this->getEntityManager()->persist($message);
        $this->getEntityManager()->flush();

        return $message;
    }
}
